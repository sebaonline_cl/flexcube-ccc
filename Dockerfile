FROM gradle:4.7.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src

ENV SERVICE_URI file:/home/gradle/src/wsdl/FCUBSAccService.wsdl

RUN ./gradlew quarkusBuild

FROM openjdk:8-jre-slim

EXPOSE 8080
RUN mkdir /build
RUN mkdir /app

COPY --from=build /home/gradle/src/build/*.jar /app/flexcube-cuenta-cargo-crear.jar

ENTRYPOINT ["java", "-jar" ,"/app/flexcube-cuenta-corriente-obtener.jar"]
