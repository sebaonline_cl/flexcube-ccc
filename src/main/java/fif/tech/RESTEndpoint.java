package fif.tech;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import fif.tech.client.SOAPClient;
import fif.tech.soap.FCUBSHEADERType;
import fif.tech.soap.CREATETRANSACTIONFSFSRES;
import fif.tech.soap.CREATETRANSACTIONFSFSREQ;

@Path("/")
public class RESTEndpoint {

    @Inject
    private SOAPClient soapClient;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public CREATETRANSACTIONFSFSRES execute(CREATETRANSACTIONFSFSREQ request) {
        FCUBSHEADERType header = new FCUBSHEADERType();

        return this.soapClient.execute(header, request);
    }
}