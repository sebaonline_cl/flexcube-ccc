package fif.tech.client;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Config {

    // ENV -> SERVICE_URI
    @ConfigProperty(name = "service.uri")
    String serviceURI;

    // ENV -> SERVICE_SSL
    @ConfigProperty(name = "service.ssl")
    boolean sslEnabled;

    @ConfigProperty(name = "override.port")
    boolean overridePort;

    @ConfigProperty(name = "service.port")
    String servicePort;

    public String getServiceURI() { return serviceURI; }

    public boolean isSslEnabled() { return sslEnabled; }

    public boolean isOverridePort() { return overridePort; }

    public String getServicePort() { return servicePort; }
}
