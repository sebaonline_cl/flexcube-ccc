package fif.tech.client;

import io.quarkus.runtime.StartupEvent;
import fif.tech.client.exception.BadGatewayException;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import fif.tech.soap.FCUBSRTService;
import fif.tech.soap.FCUBSHEADERType;
import fif.tech.soap.CREATETRANSACTIONFSFSRES;
import fif.tech.soap.CREATETRANSACTIONFSFSREQ;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.WebApplicationException;
import javax.xml.ws.WebServiceException;
import java.io.IOException;
import java.net.URL;
import javax.inject.Inject;

@ApplicationScoped
public class SOAPClient {

    @Inject
    private Config config;

    private FCUBSRTService client;
    // private PORT port;

    void onStart(@Observes StartupEvent evt) throws IOException {

        URL url = new URL(config.getServiceURI());
        if (config.isSslEnabled()) {
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            url = con.getURL();
        }

        this.client = new FCUBSRTService(url);
        // this.port = this.client.getPORT_NAME();

        // if (config.isOverridePort()) {
        //    Map<String, Object> requestContext = ((BindingProvider) port).getRequestContext();
        //    requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, config.getServicePort());
        // }
    }

    public CREATETRANSACTIONFSFSRES execute(FCUBSHEADERType header, CREATETRANSACTIONFSFSREQ request) {

        try {
              return this.client.getFCUBSRTServiceSEI().createTransactionFS(request);
           // return this.port.GENERATED_CLASS_OPERATION().execute(header, request);
        } catch (WebServiceException serverException) {

            throw new BadGatewayException(serverException.getMessage());
        }catch (Exception e) {
            throw new WebApplicationException(e.getMessage());

        }
    }
}
