package fif.tech.client.exception;


import io.quarkus.runtime.util.StringUtil;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class BadGatewayException extends  WebApplicationException {

    public BadGatewayException(final String message) {
        super(message, Response.Status.BAD_GATEWAY);
    }

    @Override
    public Response getResponse() {
        return Response
                .fromResponse(super.getResponse())
                .entity(new ErrorMessage(this.getMessage()))
                .build();
    }
}
